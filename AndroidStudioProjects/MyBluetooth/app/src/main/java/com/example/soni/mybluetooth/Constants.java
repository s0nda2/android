package com.example.soni.mybluetooth;

import java.util.UUID;

/**
 * Created by Soni on 08.06.16.
 */
public interface Constants {

    UUID MY_UUID = UUID.fromString("100001101-0000-1000-8000-00805f9b34fb");

    int REQUEST_ENABLE_BLUETOOTH = 0x123;

    String LOG_D = "soni.mybluetooth#Debug";

    String LOG_E = "soni.mybluetooth#Error";

    String OUTPUT_STREAM = "soni.mybluetooth#Output";

}
