package com.example.soni.mybluetooth;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.ParcelUuid;
import android.os.Parcelable;
import android.provider.SyncStateContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;


/**
 *
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    //public final static float vRadius = 20.f;

    private Button btn_connect, btn_next, btn_leftForward, btn_leftBackward,
            btn_rightForward, btn_rightBackward, btn_back, btn_stopMotors,
            btn_rotate_left, btn_rotate_right;

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothDevice mBluetoothRemoteDevice;
    private BluetoothSocket mBluetoothSocket;

    private boolean isBTenabled = false;
    private boolean isConnected = false;
    private OutputStream mOutputStream;
    private InputStream mInputStream;

    private ImageView mBluetoothIcon;

    private EditText editText_BT_MAC;
    private TextView mConnectionState;
    private TextView mTextHint;
    private TextView mSpeedDisplay;
    private CheckBox chkAutoBalancing;
    private CheckBox chkFullSpeed;

    private boolean isAutoBalancing = true;

    private static final int speedStep = 3;
    private int tacho;
    private int oldId;

    /**
     *
     */
    private void init() {
        mConnectionState = (TextView) findViewById(R.id.text_connection_state);
        mTextHint = (TextView) findViewById(R.id.text_hint);
        mSpeedDisplay = (TextView) findViewById(R.id.text_speed_display);

        btn_connect = (Button) findViewById(R.id.btn_connect);
        btn_next = (Button) findViewById(R.id.btn_next);
        btn_leftForward = (Button) findViewById(R.id.btn_leftForward);
        btn_leftBackward = (Button) findViewById(R.id.btn_leftBackward);
        btn_rightForward = (Button) findViewById(R.id.btn_rightForward);
        btn_rightBackward = (Button) findViewById(R.id.btn_rightBackward);
        btn_back = (Button) findViewById(R.id.btn_back);
        btn_stopMotors = (Button) findViewById(R.id.btn_stopMotors);
        btn_rotate_left = (Button) findViewById(R.id.btn_rotate_left);
        btn_rotate_right = (Button) findViewById(R.id.btn_rotate_right);

        chkAutoBalancing = (CheckBox) findViewById(R.id.checkbox_auto_balance);
        chkAutoBalancing.setChecked(true);
        chkFullSpeed = (CheckBox) findViewById(R.id.checkbox_fullspeed);

        //btn_leftForward.setEnabled(true);
        btn_leftForward.setClickable(true);
        btn_leftBackward.setClickable(true);
        btn_rightForward.setClickable(true);
        btn_rightBackward.setClickable(true);
        btn_stopMotors.setClickable(true);
        btn_rotate_left.setClickable(true);
        btn_rotate_right.setClickable(true);
        chkAutoBalancing.setClickable(true);
        chkFullSpeed.setClickable(true);

        btn_connect.setOnClickListener(this);
        btn_next.setOnClickListener(this);

        btn_leftForward.setOnClickListener(this);
        btn_leftBackward.setOnClickListener(this);
        btn_rightForward.setOnClickListener(this);
        btn_rightBackward.setOnClickListener(this);

        btn_back.setOnClickListener(this);
        btn_stopMotors.setOnClickListener(this);

        btn_rotate_left.setOnClickListener(this);
        btn_rotate_right.setOnClickListener(this);
        chkAutoBalancing.setOnClickListener(this);
        chkFullSpeed.setOnClickListener(this);

        /*
        btn_leftForward.setOnTouchListener(this);
        btn_leftBackward.setOnTouchListener(this);
        btn_rightForward.setOnTouchListener(this);
        btn_rightBackward.setOnTouchListener(this);
        */

        btn_next.setEnabled(false);
        mBluetoothIcon = (ImageView) findViewById(R.id.img_bluetooth_icon);

        // Initiates Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
        // Get the display dimension
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Log.d(Constants.LOG_D, "DISPLAY WIDTH " + size.x);

        ImageView imgView = (ImageView) findViewById(R.id.img_ball_e_001);
        Bitmap bm = ((BitmapDrawable) imgView.getDrawable()).getBitmap();

        BitmapShader shader = new BitmapShader(bm, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setShader(shader);

        // Get the size of image
        Log.d(Constants.LOG_D, "BITMAP Width: " + bm.getWidth());
        RectF rect = new RectF(0.0f, 0.0f, bm.getWidth(), bm.getHeight());
        Canvas canvas = new Canvas();

        canvas.drawRoundRect(rect, vRadius, vRadius, paint);
        */

        init();

		/*
		 * Source: https://developer.android.com/guide/topics/connectivity/bluetooth#SettingUp
		 */
        if ( mBluetoothAdapter == null ) {
            CharSequence msg = "No Bluetooth dapter could be found !";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            Log.d(Constants.LOG_D, msg.toString());
            return;
        }

        if ( !mBluetoothAdapter.isEnabled() ) {
            Intent enableBluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetoothIntent, Constants.REQUEST_ENABLE_BLUETOOTH);
        } else {
           isBTenabled = true;
        }

        if ( isBTenabled )   mBluetoothIcon.setVisibility(View.VISIBLE);
        else   mBluetoothIcon.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelBluetooth();
    }


    /**
     *
     */
    private void sendCommand(char c) {
        try {
            Log.d(Constants.LOG_D, "............................. Key " + c + " is pressed.");
            mOutputStream.write(c);
        } catch ( IOException e ) {}
    }


    /**
     *
     */
    protected void cancelBluetooth() {
        isConnected = false;
        try {
            if ( mBluetoothSocket != null ) {
                mBluetoothSocket.close();
            }
            mBluetoothSocket = null;
            Log.d(Constants.LOG_D, "BluetoothSocket: Socket closed.");
            Log.d(Constants.LOG_D, "Bluetooth (Connection): Disconnected !");
        } catch ( IOException e ) {}
    }

    /**
     * Source: https://developer.android.com/guide/topics/connectivity/bluetooth#SettingUp
     */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
    private void connectBluetooth() {
        Context ctx = this.getApplicationContext();
        if ( !isBTenabled ) {
            String msg = "Bluetooth device is disabled !";
            Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
            Log.d(Constants.LOG_D, msg);
            return;
        }

        editText_BT_MAC = (EditText) findViewById(R.id.edit_text_mac);
        String BT_MAC = editText_BT_MAC.getText().toString().trim();

        if ( !mBluetoothAdapter.checkBluetoothAddress(BT_MAC) ) {
            String msg = "The given MAC address is not valid !";
            Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
            Log.d(Constants.LOG_D, msg);
            return;
        }

        // Retrieve and get an instance of the remote Bluetooth device.
        try {
            mBluetoothRemoteDevice = mBluetoothAdapter.getRemoteDevice(BT_MAC);
            Log.d(Constants.LOG_D, "BluetoothAdapter: Remote BT device reference obtained.");
        } catch ( IllegalArgumentException e ) {
            Log.e(Constants.LOG_E, "BluetoothAdapter: Attempt to obtain an instance of remote BT device failed.");
            Log.e(Constants.LOG_E, e.getMessage());
        }

        /*
        // Intermediate step to discover the appropriate UUID's of the remote end device.
        / Requires 'minSdk=11' and 'targetSdk=11' --> Change in AndroidManifest.XML
        if ( mBluetoothRemoteDevice != null ) {
            boolean b = mBluetoothRemoteDevice.fetchUuidsWithSdp();
            if ( b ) {
                ParcelUuid[] uuids = mBluetoothRemoteDevice.getUuids();
                for ( int i = 0; i < uuids.length; i++ ) {
                    Log.d(Constants.LOG_D, "UUID " + (i+1) +  uuids[i].toString());
                }
            }
        }
        */

        // Get a BluetoothSocket to connect with the given remote device.
        try {
            mBluetoothSocket = mBluetoothRemoteDevice.createInsecureRfcommSocketToServiceRecord(Constants.MY_UUID);
            Log.d(Constants.LOG_D, "BluetoothSocket: Bluetooth socket created.");
        } catch ( IOException e ) {
            Log.e(Constants.LOG_E, "BluetoothSocket: Unable to create a Bluetooth socket.");
            Log.e(Constants.LOG_E, e.getMessage());
        }

        // Cancel discovery, since otherwise it will slow down the connection.
        mBluetoothAdapter.cancelDiscovery();

        // Connect the (remote) device through the socket
        try {
            if ( !mBluetoothSocket.isConnected() ) {
                mBluetoothSocket.connect();
                isConnected = true;
            }
            Log.d(Constants.LOG_D, "BluetoothSocket (Connection): Connection established.");
        } catch ( IOException e ) {
            // Unable to connect. Close the socket and exit.
            Log.e(Constants.LOG_E, "BluetoothSocket (Connection): Unable to create a Bluetooth connection.");
            Log.e(Constants.LOG_E, e.getMessage());
            Log.e(Constants.LOG_E, e.toString());
            e.printStackTrace();
            try {
                mBluetoothSocket.close();
                Log.d(Constants.LOG_D, "BluetoothSocket (Connection): Bluetooth socket closed.");
            } catch ( IOException e2 ) {
                Log.e(Constants.LOG_E, "BluetoothSocket (Connection): Unable to close Bluetooth socket.");
                Log.e(Constants.LOG_E, e2.getMessage());
            }
            return;
        }

        // If the connection is successful, create Input- and OutputStream to transfer data.
        // Output stream
        try {
            mOutputStream = mBluetoothSocket.getOutputStream();
            Log.d(Constants.LOG_D, "BluetoothSocket (Output Stream): Output stream obtained.");
        } catch ( IOException e ) {
            isConnected = false;
            Log.e(Constants.LOG_E, "BluetoothSocket (Output Stream): Unable to get output stream.");
            Log.e(Constants.LOG_E, e.getMessage());
        }

        // Input stream
        try {
            mInputStream = mBluetoothSocket.getInputStream();
            Log.d(Constants.LOG_D, "BluetoothSocket (Input Stream): Input stream obtained.");
        } catch ( IOException e ) {
            isConnected = false;
            Log.e(Constants.LOG_E, "BluetoothSocket (Input Stream): Unable to get input stream.");
            Log.e(Constants.LOG_E, e.getMessage());
        }

        if ( isConnected ) {
            mConnectionState.setText("Bluetooth Connection succeeded.");
            btn_connect.setText("Disconnect");
            btn_connect.setTextColor(Color.RED);
            btn_next.setEnabled(true);
            Log.d(Constants.LOG_D, "Bluetooth (Connection): Successful !");
        } else {
            mConnectionState.setText("Not connected !");
            Log.e(Constants.LOG_E, "Bluetooth (Connection): Failed !");
        }

    }


    /**
     *
     */
    private void switchCompoments(int k) {
        if ( k == 0 ) {
            editText_BT_MAC.setVisibility(View.INVISIBLE);
            btn_connect.setVisibility(View.INVISIBLE);
            mConnectionState.setVisibility(View.INVISIBLE);
            btn_next.setVisibility(View.INVISIBLE);
            mBluetoothIcon.setVisibility(View.INVISIBLE);
            mTextHint.setText(R.string.text_hint_bluetooth);

            btn_leftForward.setVisibility(View.VISIBLE);
            btn_leftBackward.setVisibility(View.VISIBLE);
            btn_rightForward.setVisibility(View.VISIBLE);
            btn_rightBackward.setVisibility(View.VISIBLE);
            btn_back.setVisibility(View.VISIBLE);
            btn_stopMotors.setVisibility(View.VISIBLE);
            btn_rotate_left.setVisibility(View.VISIBLE);
            btn_rotate_right.setVisibility(View.VISIBLE);
            mSpeedDisplay.setVisibility(View.VISIBLE);

            chkAutoBalancing.setVisibility(View.VISIBLE);
            //chkFullSpeed.setVisibility(View.VISIBLE);
        } else { // k == 1
            editText_BT_MAC.setVisibility(View.VISIBLE);
            btn_connect.setVisibility(View.VISIBLE);
            mConnectionState.setVisibility(View.VISIBLE);
            mBluetoothIcon.setVisibility(View.VISIBLE);
            mTextHint.setText(R.string.text_hint);

            btn_next.setVisibility(View.VISIBLE);
            btn_leftForward.setVisibility(View.INVISIBLE);
            btn_leftBackward.setVisibility(View.INVISIBLE);
            btn_rightForward.setVisibility(View.INVISIBLE);
            btn_rightBackward.setVisibility(View.INVISIBLE);
            btn_back.setVisibility(View.INVISIBLE);
            btn_stopMotors.setVisibility(View.INVISIBLE);
            btn_rotate_left.setVisibility(View.INVISIBLE);
            btn_rotate_right.setVisibility(View.INVISIBLE);
            mSpeedDisplay.setVisibility(View.INVISIBLE);

            chkAutoBalancing.setVisibility(View.INVISIBLE);
            //chkFullSpeed.setVisibility(View.INVISIBLE);
        }
    }

    /**
     *
     * @param mId
     */
    private void displaySpeed(int mId) {
        String strMAX = "";
        tacho = ( mId == oldId ) ? tacho + 1 : 1;
        if ( tacho > speedStep ) {
            tacho = speedStep;
            strMAX += " (MAX)";
        }
        oldId = mId;
        mSpeedDisplay.setText("Speed: " + tacho + strMAX);
    }

    @Override
    public void onClick(View view) {
        int mId = view.getId();
        switch (mId) {
            case R.id.btn_connect:
                if ( mBluetoothSocket == null ) {
                    connectBluetooth();
                } else {
                    cancelBluetooth();
                    sendCommand('a'); // Turn Auto-Balancing on
                    ((Button) view).setText("Connect");
                    mConnectionState.setText("Not connected !");
                    btn_next.setEnabled(false);
                }
                break;
            case R.id.btn_next:
                switchCompoments(0);
                break;

            case R.id.btn_leftForward:
                sendCommand('q');
                displaySpeed(R.id.btn_leftForward);
                break;
            case R.id.btn_leftBackward:
                sendCommand('r');
                displaySpeed(R.id.btn_leftBackward);
                break;
            case R.id.btn_rightForward:
                sendCommand('t');
                displaySpeed(R.id.btn_rightForward);
                break;
            case R.id.btn_rightBackward:
                sendCommand('w');
                displaySpeed(R.id.btn_rightBackward);
                break;

            case R.id.btn_stopMotors:
                sendCommand('x');
                tacho = 0;
                mSpeedDisplay.setText("Speed: " + tacho);
                break;
            case R.id.checkbox_auto_balance:
                Log.d(Constants.LOG_D, "IS CHECKED................................ : " + chkAutoBalancing.isChecked());
                if ( chkAutoBalancing.isChecked() ) {
                    sendCommand('a'); // Turn Auto-Balancing on
                } else {
                    sendCommand('d'); // Turn Auto-Balancing off
                }
                break;
            case R.id.checkbox_fullspeed:
                if ( chkFullSpeed.isChecked() ) {
                    sendCommand('f'); // Turn Full Speed mode on
                } else {
                    sendCommand('y'); // Turn Full Speed mode off
                }
                break;

            case R.id.btn_rotate_left:
                sendCommand('k');
                displaySpeed(R.id.btn_rotate_left);
                break;
            case R.id.btn_rotate_right:
                sendCommand('l');
                displaySpeed(R.id.btn_rotate_right);
                break;

            default: // bnt_back
                switchCompoments(1);
                // super.finish(); // Exit current Activity and back to previous Activity.
                break;
        }
    }




    private boolean bMotion = false;
    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch ( view.getId() ) {
            case R.id.btn_leftForward:
                switch ( event.getAction() ) {
                    case MotionEvent.ACTION_DOWN: // Press
                        sendCommand('q');
                        bMotion = true;
                        break;
                    case MotionEvent.ACTION_UP: // Release
                        bMotion = true;
                        break;
                }
                break;
            case R.id.btn_leftBackward:
                switch ( event.getAction() ) {
                    case MotionEvent.ACTION_DOWN: // Press
                        sendCommand('r');
                        bMotion = true;
                        break;
                    case MotionEvent.ACTION_UP: // Release
                        bMotion = true;
                        break;
                }
                break;
            case R.id.btn_rightForward:
                switch ( event.getAction() ) {
                    case MotionEvent.ACTION_DOWN: // Press
                        sendCommand('t');
                        bMotion = true;
                        break;
                    case MotionEvent.ACTION_UP: // Release
                        bMotion = true;
                        break;
                }
                break;
            case R.id.btn_rightBackward:
                switch ( event.getAction() ) {
                    case MotionEvent.ACTION_DOWN: // Press
                        sendCommand('w');
                        bMotion = true;
                        break;
                    case MotionEvent.ACTION_UP: // Release
                        bMotion = true;
                        break;
                }
                break;
        }
        return bMotion;
    }

    /**
     *
     */
    public void showAbout(View view) {
        String msg = "Team members: Lucas, Berni, Sami, Abdelkader, Benji, Emre, Tobi, Irem, "
                        + "Vieti, Zoe, Ivan, Haidon, Soni.";
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }
}

