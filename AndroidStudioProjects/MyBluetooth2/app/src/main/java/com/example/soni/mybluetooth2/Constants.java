package com.example.soni.mybluetooth2;

/**
 * Created by soni on 18.06.16.
 */
public interface Constants {

    String LOG_D = "soni.mybluetooth#LOG_DEBUG";
    String LOG_E = "soni.mybluetooth#LOG_ERROR";
}
