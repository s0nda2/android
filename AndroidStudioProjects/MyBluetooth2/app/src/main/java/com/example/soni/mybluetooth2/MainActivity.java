package com.example.soni.mybluetooth2;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String[] IMG_RES_NAMES = {"main_theme", "main_theme2", "main_theme3"};
    private static final int[] BUTTON_IDs = { R.id.btn_main_theme, R.id.btn_main_theme2, R.id.btn_main_theme3 };

    private ImageView img_main_theme;
    private Button[] btns; // colored buttons for theme switching
    private Button btn_start;
    private Button btn_team_members;

    private Handler handler;
    private Slider slider;
    private Thread sliderThread;

    private int currentStoppedButtonIdx = 0;


    /**
     *
     */
    class Slider implements Runnable {
        /*
         * https://wuselsnej.wordpress.com/coding/android/android-beispiel-fur-die-verwendung-von-threads/
         */
        private boolean stopped = false;
        private int current = 0;

        @Override
        public void run() {
            while ( true ) {
                // Only the Main thread that created this slider thread can touch its views.
                // So we use Handler to get access to them.
                if ( stopped ) {
                    try {
                        Thread.sleep(500);
                    } catch ( InterruptedException e ) {}
                    continue; // get out of the current (while) loop
                }
                // Use handler to access to Main thread's views
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        // Change theme image
                        int resId = MainActivity.this.getResources().getIdentifier(IMG_RES_NAMES[current], "drawable", MainActivity.this.getPackageName());
                        img_main_theme.setImageResource(resId);
                        // Decrease appearance of current button
                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) btns[current].getLayoutParams();
                        params.width -= 8;
                        params.height -= 8;
                        btns[current].setLayoutParams(params);
                        // Increase next button appearance
                        ++current;
                        if ( current == IMG_RES_NAMES.length )   current = 0;
                        params = (LinearLayout.LayoutParams) btns[current].getLayoutParams();
                        params.width += 8;
                        params.height += 8;
                        btns[current].setLayoutParams(params);
                    }
                });
                try {
                    Thread.sleep(1800);
                } catch ( InterruptedException e ) {};
            }
        }

        public int setStop(boolean b) {
            this.stopped = b;
            currentStoppedButtonIdx = ( current == IMG_RES_NAMES.length ) ? 0 : current;
            return currentStoppedButtonIdx;
            // count is the index of current theme being displayed
        }

        public void restart() {
            this.stopped = false;
        }

        public boolean isStopped() {
            return this.stopped;
        }
    }

    /**
     *
     */
    private void init() {
        img_main_theme= (ImageView) findViewById(R.id.img_main_theme);
        int bl = BUTTON_IDs.length;
        btns = new Button[bl];
        for ( int i = 0; i < bl; i++ ) {
            btns[i] = (Button) findViewById(BUTTON_IDs[i]);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) btns[i].getLayoutParams();
            params.width = 24;
            params.height = 24;
            btns[i].setLayoutParams(params);
            btns[i].setOnClickListener(this);
        }
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) btns[0].getLayoutParams();
        params.width += 8;
        params.height += 8;
        btns[0].setLayoutParams(params);

        btn_start = (Button) findViewById(R.id.btn_start);
        btn_start.setOnClickListener(this);
        btn_team_members = (Button) findViewById(R.id.btn_team_mebers);
     }


    /**
     *
     */
    private void startRemoteControlActivity() {
        Intent intent = new Intent(this, RemoteControl.class);
        currentStoppedButtonIdx = slider.setStop(true);
        sliderThread.interrupt();
        startActivity(intent);
    }


    /**
     *
     */
    private void setMainTheme(int idx) {
       int resId = this.getResources().getIdentifier(IMG_RES_NAMES[idx],
                                    "drawable", this.getPackageName());
        this.img_main_theme.setImageResource(resId);
    }

    /**
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_main_theme: // 0
                if ( currentStoppedButtonIdx == 0 && slider.isStopped() ) {
                    slider.restart();
                } else {
                    setMainTheme(slider.setStop(true));
                }
                break;
            case R.id.btn_main_theme2: // 1
                if ( currentStoppedButtonIdx == 1 && slider.isStopped() ) {
                    slider.restart();
                } else {
                    setMainTheme(slider.setStop(true));
                }
                break;
            case R.id.btn_main_theme3: // 2
                if ( currentStoppedButtonIdx == 2 && slider.isStopped() ) {
                    slider.restart();
                } else {
                    setMainTheme(slider.setStop(true));
                }
                break;
            case R.id.btn_start: // Button Start
                startRemoteControlActivity();
                break;
        }
    }

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initiate view elements
        init();

        // Initiate handler and slider thread
        handler = new Handler();
        slider = new Slider();
        sliderThread = new Thread(slider);
        sliderThread.start();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        slider.setStop(true);
        sliderThread.interrupt();
        sliderThread = null;
    }

    /**
     *
     */
    public void showTeamMembers(View view) {
        currentStoppedButtonIdx = slider.setStop(true);
        Log.d(Constants.LOG_D, "Slider Thread has been paused by Soni :))");
        String msg = "Ball-E Team: Lucas, Berni, Sami, Abdelkader, Benji, Emre, Tobi, Irem,"
                     + "Viet-Anh, Ivan, Haidon, Zoe, Soni";
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }
}


