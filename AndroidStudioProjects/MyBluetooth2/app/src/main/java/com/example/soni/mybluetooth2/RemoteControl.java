package com.example.soni.mybluetooth2;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.ViewGroup;
import android.widget.ImageView;

public class RemoteControl extends AppCompatActivity {

    private final static int[] SlideImgView_Ids =
            { R.id.img_ball_e_001, R.id.img_ball_e_002, R.id.img_ball_e_003, R.id.img_ball_e_004 };

    private ImageView[] slideImgViews;

    /**
     * This method sets width and adjusts the size of image to fit parent's bounds.
     */
    private void setParamsWidthForSlideImgViews(int w) {
        for ( ImageView iv : slideImgViews ) {
            ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) iv.getLayoutParams();
            params.width = w; // no need to change the height
            iv.setLayoutParams(params); /**/
        }
    }

    /**
     *
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void init() {
        slideImgViews = new ImageView[SlideImgView_Ids.length];
        for ( int i = 0; i < slideImgViews.length; i++ ) {
            slideImgViews[i] = (ImageView) findViewById(SlideImgView_Ids[i]);
        }

        // Determine display (screen) size.
        Display mDisplay = getWindowManager().getDefaultDisplay();

        //Point mSize = new Point();
        //mDisplay.getSize(mSize); // mDisplay stores size information in mSize.
        // getSize() available in API 13.

        DisplayMetrics mMetrics = new DisplayMetrics(); // getMetrics() available from API 1.
        mDisplay.getMetrics(mMetrics); // stores metrics information in mMetrics

        int mDisplayWidth = mMetrics.widthPixels; // mSize.x
        int mDisplayHeight = mMetrics.heightPixels; // mSize.y
        Log.d(Constants.LOG_D, "Display size: " + mDisplayWidth + "(width), " + mDisplayHeight + "(height)");

        // Adjust image width to display (480, 720, 768)
        setParamsWidthForSlideImgViews(mDisplayWidth / slideImgViews.length);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote_control);

        init();
    }
}
